# Repositories

```plantuml
@startuml

interface QuoteSourceRepository {
	+GetQuoteSources() : Collection<QuoteSource>
	+QueryQuoteSources(query : Predicate<QuoteSource>) : Collection<QuoteSource>
	+GetQuoteSource(id : int) : QuoteSource?
	+GetQuoteSource(name : string) : QuoteSource?
	+Save(QuoteSource) : bool
}

@enduml
```