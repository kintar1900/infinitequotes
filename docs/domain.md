# Domain Model

```plantuml
@startuml

class QuoteSource {
	Id : int
	Name : string
}

class QuoterPermission {
	QuoteSourceId : int
	Grant : Permission
}

enum Permission {
	SourceAdmin
	Moderator
	Contributor
	Quoter
}

class Quoter {
	Id : int
	FullName : string
	Initials : string
	IsSiteAdmin : bool
	PrimaryEmail : string
	EmailAddresses : Set<string>
	Permissions : Set<QuoterPermission>
}

class QuoteLine {
	Initials : string
	Text : string
}

class Quote {
	Id : int
	QuoteSourceId : int
	ContributingQuoterId : int
	DateContributed : DateTimeOffset
	DateQuoted : DateTimeOffset
	Lines : List<QuoteLine>
}

Quote *-- QuoteLine
Quoter *-- QuoterPermission

QuoteSource -[hidden]> Permission
QuoteSource -[hidden]- Quoter
Permission -[hidden]- Quote

@enduml
```