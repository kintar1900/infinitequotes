# Infinite Quotes

> What, again?

So the last version didn't pan out.  Event Sourcing is definitely overkill for this kind of project.  I knew that going in, but didn't realize what a time drag it would be until I got to the point of implementing the read models.  Eeesh.  As a coworker of mine used to say, "the juice just isn't worth the squeeze".

We're going with a simpler model this time.  I'll still be documenting it as I go, but I'll be breaking down the issues and milestones on the board a little better.  Hopefully that will provide easier to follow progress, too.