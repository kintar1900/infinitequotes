# Infinite Quotes

A simple serverless (AWS Lambda/S3) webapp for posting and browsing amusing or inspiring quotations.

Feel free to browse the issue board and milestones.  Drop me a line if you're interested or curious about this project at `kintar1900` at `gmail` doot `com`.

# Documentation

Documentation can be found [here](https://docs.thequotewall.com).

# Development Status

[![pipeline status](https://gitlab.com/kintar1900/infinitequotes/badges/develop/pipeline.svg)](https://gitlab.com/kintar1900/infinitequotes/commits/develop)
[![coverage report](https://gitlab.com/kintar1900/infinitequotes/badges/develop/coverage.svg)](https://gitlab.com/kintar1900/infinitequotes/commits/develop)
