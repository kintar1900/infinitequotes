namespace QuoteWall.Model
{
    public enum Permission
    {
        SourceAdmin,
        Moderator,
        Contributor,
        Quoter
    }
}