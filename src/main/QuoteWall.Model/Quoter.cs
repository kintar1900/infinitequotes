using System.Collections.Generic;
using System.Collections.Immutable;

namespace QuoteWall.Model
{
    public class Quoter
    {
        public string FullName { get; private set; }
        public string Initials { get; private set; }
        public bool IsDisabled { get; private set; }
        public bool IsSiteAdmin { get; private set; }
        private ICollection<string> emailAddresses = new HashSet<string>();
        
        public ICollection<string> EmailAddresses => emailAddresses.ToImmutableList();
        public string PrimaryEmail { get; private set; }
    }
}