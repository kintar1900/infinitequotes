namespace QuoteWall.Model
{
    public class QuoteSource
    {
        public int Id { get; private set; }
        public string Name { get; private set; }

        public QuoteSource(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }

}